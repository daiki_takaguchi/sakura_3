﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace SAKURA.GAMEPLAY.DUNGEON{
	public class Enemy_object : MonoBehaviour {

		public Transform position_transform;
		public Transform rotation_transform;

		public Transform range_left;
		public Transform angle_left;

		public Transform range_right;
		public Transform angle_right;

		public Light sight_light;

		public void set_angle_of_view (float input_angle){

			angle_left.localRotation = Quaternion.Euler(new Vector3(0,input_angle/2.0f,0));
			angle_right.localRotation = Quaternion.Euler(new Vector3(0,-input_angle/2.0f,0));

			sight_light.spotAngle = input_angle;

		}

		public void set_range_of_view (float input_range){
			range_left.localPosition = new Vector3 (0, 0, input_range / 2.0f);
			range_left.localScale = new Vector3 (0.1f, 1, input_range);

			range_right.localPosition = new Vector3 (0, 0, input_range / 2.0f);
			range_right.localScale = new Vector3 (0.1f, 1, input_range);
		}

		/*
		public Vector3 get_local_position(){
			return position_transform.localPosition;
		}

		public void set_local_position(Vector3 input_position){
			position_transform.localPosition = input_position;
		}*/

		/*
		public Quaternion get_local_rotation(){

			return rotation_transform.localRotation;
		}

		public void set_local_rotation(Quaternion input_rotation){
			rotation_transform.localRotation = input_rotation;
		}*/
	
	}
}