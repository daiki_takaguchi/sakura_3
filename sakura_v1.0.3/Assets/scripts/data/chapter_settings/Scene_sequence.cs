﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_sequence :Adv_settings_base {

		public int chapter_no;
		public string scene_name;
		///public int parallel_no;
		public int sequence_no;

		public Scene_settings scene_settings;

		public List<Scene_sequence_state> states;

		public int current_state_pointer=-1;
		public Scene_sequence_state current_state;


		[HideInInspector] public string current_state_name;


		// Use this for initialization
		void Start () {
		
		}


		
		// Update is called once per frame
		void Update () {
			
			if (!Application.isPlaying) {
				if(variables_current_scope==null){
					GameObject temp = new GameObject();
					temp.transform.parent=transform;
					variables_current_scope=temp.AddComponent<Variable_list>();
					
				}

				if(current_state==null){
					if(states.Count>0){
						current_state=states[0];
						current_state_name=states[0].name;
					}
				}

			}
		}

		public void run(){
			current_state.run ();
		}


		public void run_transition(){
			current_state_pointer++;

			if(current_state_pointer<states.Count){
				current_state=states[current_state_pointer];
				current_state_name=states[current_state_pointer].name;
			}else{

			}
		}

	}
}
