﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.CONDITION;

namespace SAKURA.DATA.SCENE_SETTINGS{
	[ExecuteInEditMode]
	public class Scene_sequence_state : Adv_settings_base {

		public int chapter_no;
		public string scene_name;
		public int sequence_no;
		public int state_no;

		public Scene_sequence scene_sequence;

		public Game_event_list game_event_list;
	
		// Use this for initialization
		void Start () {
		
		}

		public void check_transition(){

		}

		public void run(){
			game_event_list.run_events ();
		}

		public void run_transition(){

		}

		public void check_events(){

		}

		
		// Update is called once per frame

	}
}
