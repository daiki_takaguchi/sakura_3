﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Change_game_state :  Action {

		/*
		public override SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.change_game_state;}
		}*/

		public SAKURA.Consts.Gameplay.Game_state_type next_game_state;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}



		public override void run(Action_list input_action_list){
			
			SAKURA.Gameplay.gameplay_static.change_game_state (next_game_state);
			
			input_action_list.run_next ();
			
		}


	}
}
