﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Wait : Action {




		public float wait_time;

		//private bool is_started = false;
		//private float start_time;

		//public SAKURA.Consts.Ui.Layer_type jump_layer_type = Consts.Ui.Layer_type.none;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}


		public override void run(Action_list input_action_list){
		
			parent_action_list = input_action_list;
			StartCoroutine (wait_for_seconds(wait_time));
		}


		public IEnumerator wait_for_seconds(float wait_time){
			yield return new WaitForSeconds(wait_time);

			parent_action_list.run_next ();
		}
	}
}
