﻿using UnityEngine;
using System.Collections;
using SAKURA.DATA.SCENE_SETTINGS;

namespace SAKURA.DATA.GAME_EVENT.ACTION{
	public class Jump_to_scene : Action {


		//public override SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type= Consts.Data.Scene_settings.Action.Action_type.None;
		/*
		public  override  SAKURA.Consts.Data.Scene_settings.Action.Action_type action_type
		{
			get { return Consts.Data.Scene_settings.Action.Action_type.jump_to_scene;}
		}*/

		//public SAKURA.Consts.Ui.Layer_type jump_layer_type = Consts.Ui.Layer_type.none;

		public Scene_settings scene_settings;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public override void run(Action_list input_action_list){

		


			Gameplay.gameplay_static.load_scene (scene_settings.gameObject);

			input_action_list.run_next ();

		}
	}
}
