﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UNITY_EDITOR;

namespace SAKURA.DATA.GAME_EVENT.ACTION{

	[ExecuteInEditMode]
	public class Action_list : MonoBehaviour {

		// Use this for initialization

		[HideInInspector] public Game_event parent_game_event;

		public bool save_data_now;
		public bool load_data_now;
		public bool load_data_confirm;

		public string data_file;

		public List<Action> action_list;

		public int current_action_pointer=0;

		//private int max_loop_count=1000;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {

			/*if(!Application.isPlaying){

				if(add_action_now){
					add_action (
					add_action_now=false;
				}
				
			}*/

			if(!Application.isPlaying){
				if(save_data_now){

					List<string> temp =new List<string>();
					temp.Add ("a");
					temp.Add ("\",\"");
					temp.Add ("b");

					string temp_2=Csv_functions.save_one_row(temp);


					print (temp_2);

					List<string> temp_3=Csv_functions.load_one_row(temp_2);

					foreach(string one_text in temp_3){
						print (one_text);
					}

					save_data_now=false;
				}
			}
		
		}

		public void save(){
		
		}


		public void load(){

		}
	

		public void refresh_children(Game_event input_parent_game_event){

			parent_game_event = input_parent_game_event;

			action_list = Sakura.get_monos_from_children<Action> (transform);

			int action_no = 0;

			foreach(Action action in action_list){
				
				action.name="action "+action_no+": "+action.GetType().Name;
				action.parent_action_list=this;
				action_no++;
			}
		}

		public T add_action<T>() where T:Action{


			GameObject temp = new GameObject ();
			temp.transform.parent = transform;
			T output = (T) temp.AddComponent<T>();
			output.parent_action_list=this;
			action_list.Add (output);

			
			refresh_children (parent_game_event);
			
			
			return output;

		}

		public void run(int pointer_shift){

			current_action_pointer+=pointer_shift;

			if(action_list.Count>current_action_pointer){
				action_list[current_action_pointer].run(this);
			}
			

		}

		public void run_next(){
			
			run (1);
		}

		public void run(){


			run (0);
		}

		public void initalize_pointer(){
			current_action_pointer = 0;
		}

	}
}
