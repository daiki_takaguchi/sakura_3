﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT.CONDITION;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT{
	[ExecuteInEditMode]
	public class Condition_list : MonoBehaviour {

		public Game_event parent_game_event;

		public List<Condition> condition_list;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame


		public void refresh_children(Game_event input_parent_game_event){

			int condition_no=0;

			parent_game_event=input_parent_game_event;

			condition_list = Sakura.get_monos_from_children<Condition> (transform);
			
			foreach(Condition condition in condition_list){
			
				condition.name="conditon "+condition_no+": "+condition.GetType ().Name;

				condition.refresh_condition(this);
				
				condition_no++;

			}
		}

		public T add_condition<T>() where T:Condition{


			GameObject temp = new GameObject ();
			temp.transform.parent = transform;
			T output = (T) temp.AddComponent<T>();
			condition_list.Add (output);

			return output;

		}

		public List<Variable> add_event_to_variables(Game_event input_game_event){

			List<Variable> output = new  List<Variable> ();

			foreach(Condition condition in condition_list){
				
				Variable temp=condition.add_event_to_variable(input_game_event);

				if(temp!=null){
					output.Add (temp);
				}
				
			}

			return output;

		}
		/*
		public Type convert_type_to_mono (Consts.Data.Game_event.Condition.Coidition_type input_condition_type) {

			Type output= typeof(Condition_none);

			return output;
		}*/

		public bool check_conditions(){

			bool output = true;

			foreach(Condition condition in condition_list){
				output&=condition.check_condition();
			}

			return output;
		}
	}
}
