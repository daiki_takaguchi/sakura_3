﻿using UnityEngine;
using System.Collections;
using SAKURA;
using SAKURA.DATA;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.DATA.GAME_EVENT.CONDITION{
	public class Condition : MonoBehaviour {

		/*
		public virtual Consts.Data.Game_event.Condition.Coidition_type condition_type {
			get {
				return Consts.Data.Game_event.Condition.Coidition_type.default_condition;
			}
				
		}*/

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public virtual void refresh_condition(Condition_list input_condition_list){
				
		}

		public virtual Variable add_event_to_variable(Game_event input_game_event){
			return null;
		}

		public virtual bool check_condition(){
			return false;
		}
	}
}
