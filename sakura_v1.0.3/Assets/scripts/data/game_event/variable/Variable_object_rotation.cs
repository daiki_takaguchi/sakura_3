﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;

namespace SAKURA.DATA.GAME_EVENT.VARIABLE{
	public class Variable_object_rotation : Variable {

		//public static Variable_backlog variable_backlog_static;
		public Vector3 initial_rotation;
		public SAKURA.Consts.Data.Game_event.Variable.Variable_object_rotation_initial_type inital_rotation_type;
		public SAKURA.Consts.Data.Game_event.Variable.Variable_object_rotation_state state;

		public Transform  object_rotation_transform;

		public float animation_start_time;
		public float animation_end_time_from_start;

		public Quaternion animation_local_rotation_from;
		
		public Quaternion animation_local_rotation_to;

		//public float animation_angle_between_start_to_end;

		//private SAKURA.Consts.Data.Scene_settings.Action.Person_name prev_person;

		// Use this for initialization
		void Start () {

		}

		void Update(){
		
			if (state == Consts.Data.Game_event.Variable.Variable_object_rotation_state.animation_on) {
				
				move_animation();
			}
		}

		public void move_animation(){
			
			bool is_animation_end = false;
			
			float time_from_start = (Time.time - animation_start_time);


			if(time_from_start<animation_end_time_from_start){
				
				float ratio = time_from_start / animation_end_time_from_start;
				
				object_rotation_transform.localRotation=
					Quaternion.Slerp(animation_local_rotation_from,animation_local_rotation_to,ratio);
				
			}else{
				is_animation_end=true;
			}


			if(is_animation_end){
				state=Consts.Data.Game_event.Variable.Variable_object_rotation_state.none;
				object_rotation_transform.localRotation=animation_local_rotation_to;
			}
		}



		public void change_object_local_rotation_animation(Quaternion input_rotation, float input_time){

			print (state);

			if(state==Consts.Data.Game_event.Variable.Variable_object_rotation_state.none){
				
				
				animation_local_rotation_from=get_object_local_rotation();
				animation_local_rotation_to=input_rotation;
				
				animation_start_time=Time.time;
				animation_end_time_from_start=input_time;

				Vector3 temp= new Vector3(0,0,1);

				
				state=Consts.Data.Game_event.Variable.Variable_object_rotation_state.animation_on;
				
			}
		}

		
		public void add_object_local_rotation(Quaternion input_rotation){
			object_rotation_transform.localRotation *= input_rotation;
		}


		public void change_object_local_rotation(Quaternion input_rotation){
			object_rotation_transform.localRotation = input_rotation;
		}


		public Quaternion get_object_local_rotation(){
			return object_rotation_transform.localRotation;
		}

	
	}
}
