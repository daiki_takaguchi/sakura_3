﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.ACTION;


namespace SAKURA.DATA {
	[ExecuteInEditMode]
	public class Dungeon_settings : MonoBehaviour {

		//public SAKURA.Consts.Ui.Scene_type scene_type;

		public Data data;

		public bool refresh_points_now=false;
		public bool is_edit_mode = false;

		//public bool add_enemy_move_point=false;

		public List<DUNGEON_SETTINGS.Enemy_settings> enemy_settings;

		//public Action[] actions;


		public float first_mental_threshold;
			
		public float second_mental_threshold;
				
		public float auditory_radius;
				
		public float stress_radius;




		// Use this for initialization
		void Start () {
			
		}
			

		// Update is called once per frame
		void Update () {

			if(refresh_points_now){

				refresh_points();

		
				refresh_points_now=false;
			}




		}

		public void refresh_points(){


			
			foreach (DUNGEON_SETTINGS.Enemy_settings enemy_setting in enemy_settings ){
				enemy_setting.set_points ();
				foreach (DUNGEON_SETTINGS.Enemy_move_point point in enemy_setting.enemy_move_points){
					if(point!=null){
						point.refresh_point();
						point.objects_active(is_edit_mode);
					}
				}
			}
			
			
			calculate_distance();
		}
			
			
			
		public void calculate_distance(){
			foreach (DUNGEON_SETTINGS.Enemy_settings enemy_setting in enemy_settings ){
				foreach (DUNGEON_SETTINGS.Enemy_move_point point in enemy_setting.enemy_move_points) {

					//print (point.name);
					if(point!=null){
						point.calculate_distance(enemy_setting.enemy_move_points);
					}
				}
			}
		}


	

	

			

	}
}
	