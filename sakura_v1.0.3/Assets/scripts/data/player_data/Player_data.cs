﻿using UnityEngine;
using System.Collections;

namespace SAKURA.DATA{
		public class Player_data : MonoBehaviour {

		public USERDATA.Savedata_unit normal_savedata;
		public USERDATA.Savedata_unit quick_savedata;

		public int text_speed_level;

		public Dungeon_settings[] current_settings;
	
		// Use this for initialization
		void Start () {

		}
			

		// Update is called once per frame
		void Update () {
		
		}

		public float get_text_speed(){

			float output=0.2f;

			if(text_speed_level>=0&&text_speed_level<Data.data_static.game_settings.text_speed.Count){

				output=Data.data_static.game_settings.text_speed[text_speed_level];

			}

			return output;
		}


	}
	
}

