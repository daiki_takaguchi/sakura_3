﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.UI.NGUI;

namespace SAKURA.UI.OBJECTS{
	public class Scroll_collider : MonoBehaviour {

		//public float offset_y;
		public Scroll scroll;
		public SAKURA.Consts.Ui.Layer_depth_type scroll_depth;

		// Use this for initialization
		void Start () {
			//limit_y_bottom = -(Screen.height/ 2) * (640.0f / Screen.width) + 200.0f;
		}
		
		// Update is called once per frame
		void Update () {}


		void OnPress (bool is_pressed) {
			scroll.pressed(is_pressed);
		}
	}
}
