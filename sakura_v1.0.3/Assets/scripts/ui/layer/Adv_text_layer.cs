﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA.GAME_EVENT;
using SAKURA.DATA.GAME_EVENT.VARIABLE;

namespace SAKURA.UI.LAYER{
	public class Adv_text_layer : Layer {

		// Use this for initialization

		public SAKURA.UI.OBJECTS.Text person_name;
		public SAKURA.UI.OBJECTS.Text main_text;

		public Variable_backlog variable_backlog;

		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}



		public override List<SAKURA.Consts.Ui.Layer_type> show(){

			gameObject.SetActive (true);

			return layers_conflicting;
		}

		public void clear_text(){
			person_name.clear_text ();
			main_text.clear_text ();
		}





	
	}
}
