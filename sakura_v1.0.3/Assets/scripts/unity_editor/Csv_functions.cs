﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SAKURA.DATA;
using System.IO;

namespace SAKURA.UNITY_EDITOR{

	public class Csv_functions : MonoBehaviour {

		private static StreamReader load_file(string file_name){

			StreamReader output= new StreamReader(file_name);

		

			return output;
		}

		private static StreamWriter write_file(string file_name, bool append){

			StreamWriter output = new StreamWriter (file_name, append);

			return output;

		}

		public static List<string> load_one_row(string one_row){
			List<string> output = new List<string> ();


			string temp = one_row;

			bool is_double_quoted_data_string = false;
			bool is_none_double_quoted_data_string = false;
			bool is_prev_char_double_quote = false;

			string row_data = "";

			for (int i = 0; i < temp.Length; i++){
				if(is_double_quoted_data_string){

					if(is_prev_char_double_quote){

						if(temp[i]=='\"'){
							is_prev_char_double_quote =false;
							row_data+="\"";

						} else if(temp[i]==','){
							output.Add (row_data);
							is_double_quoted_data_string=false;

						}else if(temp[i]=='\r' ||temp[i]=='\n'){
							output.Add (row_data);
							break;

						}else{
							Debug.LogError("csv parse error!");
						}
					}else{

						if(temp[i]=='\"'){
							is_prev_char_double_quote =true;

						}else{
							is_prev_char_double_quote =false;
							row_data+=temp[i];
						}
					}

				}else{

					is_prev_char_double_quote =false;

					if(temp[i]=='\"'){
						row_data="";

						is_double_quoted_data_string=true;

					
					}else{
						Debug.LogError("csv parse error! at char "+temp[i]);
					}

				}

			}


			return output;


		}

		public static List<string> split_by_first_comma(string input_string){
			List<string> output = new List<string> ();



			return output;

		}

		public static string save_one_row(List<string> one_row){
			string output = "\"";

			bool is_first_row = true;

			foreach(string one_data in one_row){

				if(is_first_row){
					is_first_row=false;
					
				}else{
					output+="\",\"";
				}

				output+=one_data.Replace("\"","\"\"");


			}

			output += "\"\r\n";

			return output;
		}


		public static List<List<string>>load(string file_name){

			List<List<string>> output = new List<List<string>> ();


			return output;

		}



		public static bool add(string file_name, List<string> data){
			bool output = true;


			return output;
		}



		public static bool over_write(string file_name, List<List<string>> data){

			bool output = true;
			
			
			return output;

		}


	}
}
