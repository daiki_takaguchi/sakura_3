﻿using UnityEngine;
using System.Collections;


namespace SAKURA.AUDIO{
	public class Se_oneshot : MonoBehaviour {

		public AudioSource source;

		//public AudioSource beat;

		// Use this for initialization
		void Start () {
		
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void play(){

			source.Play ();

		}

		public void change_pan(float input_pan){
			source.panStereo = input_pan;
			//print ("Pan:"+input_pan);
		}

		public void change_volume(float input_volume){
			source.volume=input_volume;
			//print ("Pan:"+input_volume);
		}
	}
}
